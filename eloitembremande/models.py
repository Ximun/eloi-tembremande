from django.db import models
from tinymce.models import HTMLField

class Projet(models.Model):
    nom = models.CharField(max_length=100, null=False, unique=True)
    instruments = models.CharField(max_length=100, null=True, blank=True, unique=False)
    description = HTMLField()
    telephone = models.CharField(max_length=50, null=True, blank=True)
    mail = models.EmailField(max_length=150, blank=True)
    website = models.URLField(max_length=150, blank=True)
    facebook = models.URLField(max_length=150, blank=True)
    ami = models.BooleanField(help_text="Est-ce un projet ami?", default=False)

    def __str__(self):
        return self.nom

class Concert(models.Model):
    projet = models.ForeignKey('Projet', on_delete=models.SET_NULL, null=True)
    date = models.DateField()
    ville = models.CharField(max_length=50, null=False)
    lieu = models.CharField(max_length=80, blank=True)
    evenement = models.URLField(max_length=150, help_text="Lien vers évènement Facebook", blank=True)

    def __str__(self):
        return self.date.strftime('%d %b %Y')

class Video(models.Model):
    projet = models.ForeignKey('Projet', on_delete=models.CASCADE)
    code = models.CharField(max_length=300, null=False, help_text="code à inscrire pour intégrer la vidéo")
