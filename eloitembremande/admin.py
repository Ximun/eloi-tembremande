from django.contrib import admin
from .models import Projet, Concert, Video

class VideoInline(admin.TabularInline):
    model = Video

class ProjetAdmin(admin.ModelAdmin):
    list_display = ('nom', 'instruments')
    inlines = [VideoInline]

class ConcertAdmin(admin.ModelAdmin):
    list_display = ('date', 'projet', 'ville', 'lieu')
    ordering = ('date',)

admin.site.register(Projet, ProjetAdmin)
admin.site.register(Concert, ConcertAdmin)
