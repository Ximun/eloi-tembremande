from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView
from . import views
from .views import ProjetsView, ConcertsView, ContactsView, AmisView

urlpatterns = [
    path('', views.home, name="home"),
    path('projets/', ProjetsView.as_view(extra_context={'title': 'Projets'}), name="projets"),
    path('agenda/', ConcertsView.as_view(extra_context={'title': 'Agenda'}), name="agenda"),
    path('photos/', TemplateView.as_view(template_name="eloitembremande/pages/photos.html", extra_context={'title': 'Photos'}), name="photos"),
    path('amis/', AmisView.as_view(extra_context={'title': 'Amis'}), name="amis"),
    path('contact/', ContactsView.as_view(extra_context={'title': 'Contact'}), name="contact"),
    path('mentions-legales/', TemplateView.as_view(template_name="eloitembremande/pages/mentions-legales.html", extra_context={'title': 'Mentions Légales'}), name="mentions-legales"),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
