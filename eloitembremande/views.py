from django.views.generic.list import ListView
from .models import Projet, Concert
from django.shortcuts import render

def home(request):
    return render(request, 'eloitembremande/pages/home.html')

class ProjetsView(ListView):
    model = Projet
    template_name = 'eloitembremande/pages/projets.html'

class ConcertsView(ListView):
    model = Concert
    template_name = 'eloitembremande/pages/agenda.html'

class AmisView(ListView):
    model = Projet
    template_name = 'eloitembremande/pages/amis.html'

class ContactsView(ListView):
    model = Projet
    template_name = 'eloitembremande/pages/contact.html'
