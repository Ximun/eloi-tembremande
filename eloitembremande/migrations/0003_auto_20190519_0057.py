# Generated by Django 2.2 on 2019-05-18 22:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eloitembremande', '0002_video'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='code',
            field=models.CharField(help_text='code à inscrire pour intégrer la vidéo', max_length=300),
        ),
    ]
