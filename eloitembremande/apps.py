from django.apps import AppConfig

class EloiConfig(AppConfig):
    name = 'eloitembremande'
    verbose_name = 'Eloi Tembremande'
