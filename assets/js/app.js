import $ from 'jquery'
import '../../node_modules/magnific-popup/dist/magnific-popup.css'
import '../../node_modules/magnific-popup/dist/jquery.magnific-popup.js'
import '../css/font-awesome.min.css'
import './modules/accordeon.js'

// Redimensionnement youtube iframes
$(document).ready(function () {
  if ((window.innerWidth < 690) && (document.querySelector('.videos'))) {
    document.querySelectorAll('iframe').forEach((e) => {
      e.setAttribute('width', '100%')
      e.setAttribute('height', '100%')
    })
  }
})

// Hauteur dynamique de la soundwave
$(document).ready(function () {
  if ($('.soundwave').length) {
    $('.soundwave').height($('.soundwave').width() * 0.29)
  }
})
