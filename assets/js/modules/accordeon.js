if (document.querySelector('.accordeon')) {
  let acc = document.querySelectorAll('.accordeon')
  acc[0].nextElementSibling.style.maxHeight = acc[0].nextElementSibling.scrollHeight + 'px'
  acc[0].classList.add('active')

  acc.forEach((e) => {
    let chevron = document.createElement('div')
    chevron.classList.add('chevron')
    chevron.innerHTML = '<i class="fa fa-caret-right"></i>'
    e.appendChild(chevron)
    e.addEventListener('click', () => {
      e.classList.toggle('active')
      let panel = e.nextElementSibling
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null
      } else {
        panel.style.maxHeight = panel.scrollHeight + 'px'
      }
    })
  })
}
