const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const ManifestPlugin = require('webpack-manifest-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const BundleTracker = require('webpack-bundle-tracker')

const dev = process.env.NODE_ENV === "dev"

let cssLoaders = [
    { loader: 'css-loader', options: { importLoaders: 1,  minimize: !dev } }
]

if (!dev) {
    cssLoaders.push({
        loader: 'postcss-loader',
        options: {
            plugins: (loader) => [
                require('autoprefixer')({
                    browsers: ['last 2 versions', 'ie >= 7']
                }),
            ]
        }
    })
}

var config = {

    entry: {
        app: ['./assets/scss/app.scss', './assets/js/app.js']
    },
    watch: dev,
    output: {
        path: path.resolve('./eloitembremande/static/bundles/'),
        filename: dev ? '[name].js' : '[name].[chunkhash:8].js',
    },
    resolve: {
        alias: {
            '@scss': path.resolve('./assets/scss'),
            '@js': path.resolve('./assets/js')
        }
    },

    devtool: dev ? "cheap-module-eval-source-map" : false,

    devServer: {
        overlay: true
    },

    module: {
        rules: [
            {
                test: require.resolve('jquery'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: '$'
                    }
                ]
            },
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['eslint-loader']
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            {
                test: /\.ejs$/,
                exclude: /(node_modules|bower_components)/,
                use: ['ejs-loader']
            },
            {
                test: /\.css$/,
                use: [
                    dev ? 'style-loader' : MiniCssExtractPlugin.loader,
                    ...cssLoaders,
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    dev ? 'style-loader' : MiniCssExtractPlugin.loader,
                    ...cssLoaders,
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff2?|eot|ttf|otf|wav)(\?.*)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: '[name].[ext]',
                            publicPath: dev ? 'static/bundles/' : ''
                        }
                    }
                ]
            }

        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: dev ? '[name].css' : '[name].[contenthash:8].css',
            disable: dev
        }),
        new BundleTracker({
          filename: './webpack-stats.json'
        })
    ]

}

if (!dev) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: false
    }))
    config.plugins.push(new ManifestPlugin())
    config.plugins.push(new CleanWebpackPlugin({
        root: path.resolve('./'),
        verbose: true,
        dry: false
    }))
}

module.exports = config
